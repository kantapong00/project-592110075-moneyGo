import React, { Component } from 'react';
import { View, Text, ImageBackground, Image, TouchableOpacity } from 'react-native';
import { style, Icon, Button, InputItem, StyleSheet } from '@ant-design/react-native'
import loginBG from '../image/bg3.jpg'
import { Container, InputBox, LoginButton, TextLogin } from '../style/BasicStyle'
import RegisterPage from './Register';


export default class LoginPage extends Component {

    state = {
        email: 'test2@gmail.com',
        password: '123456',
        isLoading: false
    }

    goToRegister = () => {
        this.props.history.push('/Register')
    }
    login = () => {
        this.props.history.push('/Main')
    }

    render() {
        return (
            <ImageBackground source={loginBG} style={{ flex: 1 }}>
                <Container>

                    <Image source={require('../image/logo1.png')} style={{ width: 300, height: 250, top: -50 }} />

                    <View style={{ flexDirection: 'row', top: -30 }}>
                        <TextLogin style={{ marginLeft: 25 }}>E-mail : </TextLogin>
                        <InputBox>
                            <InputItem style={{ top: -2, fontSize: 15 }}
                                value={this.state.email}
                                onChange={value => this.setState({ email: value })} />
                        </InputBox>
                    </View>

                    <View style={{ flexDirection: 'row', top: -20 }}>
                        <TextLogin>Password :</TextLogin>
                        <InputBox style={{left: 2}}>
                            <InputItem style={{ top: -2, fontSize: 15 }}
                                type='password'
                                value={this.state.password}
                                onChange={value => { this.setState({ password: value }) }}
                                secureTextEntry={true} />
                        </InputBox>
                    </View>

                    <LoginButton style={{ margin: 10,backgroundColor:'black' }} onPress={this.login}>
                        <Text style={{ color: 'orange' }}>Login</Text>
                    </LoginButton>

                    <View style={{ flexDirection: 'row', top: 60 }}>
                        <Text style={{color:'white'}}>Already have an account yet?</Text>
                        <TouchableOpacity
                            onPress={this.goToRegister}
                        >
                            <Text style={{ color: 'orange', fontSize: 18, top: -4 }}> Signup</Text>
                        </TouchableOpacity>
                    </View>

                </Container>
            </ImageBackground >
        )
    }
}


