import React, { Component } from 'react';
import { StyleSheet, Text, View, FlatList, ImageBackground, TouchableOpacity, TextInput, Image, ScrollView } from 'react-native';
import { Icon, TabBar } from '@ant-design/react-native';
import loginBG from '../image/bg3.jpg'
import { Body, MoneyBox, MoneyText, Top } from '../style/BasicStyle';
import ProfilePage from './Profile';


export default class MainPage extends Component {
    state = {
        selectedTab: 'first',
        wallet: {
            _id: '',
            owner: '',
            name: '',
            balance: 0,
            transactions: []
        }
    }

    goToMain = () => {
        this.props.history.push('/Main')
    }
    goToProfile = () => {
        this.props.history.push('/Profile')
    }

    addwallet = () => {
        this.props.history.push('/Addwallet')
    }

    UNSAFE_componentWillMount() {
        console.log('props:', this.props);
        console.log('FETCH WALLETS:', this.props.wallets);
        this.setState({ wallets: this.props.wallets })
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <ImageBackground source={loginBG} style={{ flex: 2 }}>

                    <Image source={require('../image/wallet-main.png')} style={{ width: '40%', height: '60%', left: 110, top: 15 }} />
                    <MoneyBox>
                        <MoneyText>0.00 THB</MoneyText>
                    </MoneyBox>

                </ImageBackground>
                <Body>
                    <View style={{ top: 15 }}>
                        <TouchableOpacity
                            onPress={this.addwallet}
                            style={{ flexDirection: 'row' }}
                        >
                            <Icon name="plus-circle" style={{ color: 'black', top: 2, right: 5 }} />
                            <Text style={{ fontSize: 20, color: 'black' }}>Add new wallet</Text>
                        </TouchableOpacity>
                    </View>

                    <ScrollView>
                        <FlatList
                            data={this.state.wallets}
                            style={{ width: '100%' }}
                            keyExtractor={(item) => item._id}
                            renderItem={({ item }) => (
                                <Item
                                    extra={Number.parseFloat(item.balance).toFixed(2)}
                                    style={styles.wallet}
                                    arrow="horizontal"
                                    onPress={() => this.goToAddmoney(item)}>

                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={{ fontFamily: 'Waffle Regular', fontSize: 30, left: 25 }}>{item.name}</Text>

                                    </View>
                                </Item>
                            )}
                        />
                    </ScrollView>


                </Body>
                <View style={{ flex: 0.45 }}>
                    <TabBar
                        unselectedTintColor="#949494"
                        tintColor="#FFA500"
                        barTintColor="black"
                    >
                        <TabBar.Item
                            title="Wallet"
                            icon={<Icon name="wallet" />}
                            selected={this.state.selectedTab === 'first'}
                            onPress={this.goToMain}
                        >
                        </TabBar.Item>
                        <TabBar.Item
                            title="Profile"
                            icon={<Icon name="user" />}
                            selected={this.state.selectedTab === 'second'}
                            onPress={this.goToProfile}
                        >
                        <ProfilePage />
                        </TabBar.Item>

                    </TabBar>
                </View>
            </View>
        )
    }
}
