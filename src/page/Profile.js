import React, { Component } from 'react';
import { View, Text, ImageBackground, Image, TouchableOpacity } from 'react-native';
import { Icon, TabBar } from '@ant-design/react-native';
import profileBG from '../image/profileBG.jpg';
import { Container } from '../style/BasicStyle';
import { IconCircle } from '../style/ProfileStyle';
import profile from '../image/profile.png'
import ImagePicker from 'react-native-image-picker';
import axios from 'axios';


export default class ProfilePage extends Component {
    state = {
        selectedTab: 'second',
    }

    goToMain = () => {
        this.props.history.push('/Main')
    }
    goToProfile = () => {
        this.props.history.push('/Profile')
    }

    logout = () => {
        return this.props.history.push('/')
    }



    render() {
        return (
            <View style={{ flex: 1 }}>
                <ImageBackground source={profileBG} style={{ flex: 5 }}>
                    <TouchableOpacity style={{ flexDirection: 'row', justifyContent: 'flex-end' }} onPress={this.logout}>
                        <Text style={{ top: 20, fontSize: 18, color: 'white' }}>Logout</Text>
                        <Image source={require('../image/logout.png')} style={{ width: 70, height: 70 }} />
                    </TouchableOpacity>
                    <Container>
                        <View style={{top: 18,left:3}}>
                            <ImageBackground source={profile} style={{ width: 160, height: 160, borderRadius: 80 }}>
                                {/* <Image source={{ uri: this.state.imagePath }} style={{ width: 150, height: 150, borderRadius: 80 }} /> */}
                            </ImageBackground>
                        </View>
                        <TouchableOpacity style={{ top:-15, left: 80 }}>
                            <IconCircle>
                                <Icon name='edit' size={30} color='orange' />
                            </IconCircle>
                        </TouchableOpacity>
                        <View style={{ left: -100, top: 10 }}>
                            <Text style={{ fontSize: 25, color: 'black' }}>My Profile</Text>
                        </View>

                        <View style={{ top: 20, left: -90 }}>
                            <Text style={{ fontSize: 18, color: 'black' }}>E-mail : </Text>
                        </View>

                        <View style={{ top: 20, left: -105 }} >
                            <Text style={{ fontSize: 18, color: 'black', marginTop: 10 }}>Firstname : </Text>
                            <Text style={{ fontSize: 18, color: 'black', marginTop: 10 }}>Lastname : </Text>
                        </View>
                    </Container>
                </ImageBackground>

                <View style={{ flex: 0.45 }}>
                    <TabBar
                        unselectedTintColor="#949494"
                        tintColor="#FFA500"
                        barTintColor="black"
                    >
                        <TabBar.Item
                            title="Wallet"
                            icon={<Icon name="wallet" />}
                            selected={this.state.selectedTab === 'first'}
                            onPress={this.goToMain}
                        >
                        </TabBar.Item>
                        <TabBar.Item
                            title="Profile"
                            icon={<Icon name="user" />}
                            selected={this.state.selectedTab === 'second'}
                            onPress={this.goToProfile}
                        >
                        </TabBar.Item>

                    </TabBar>
                </View>
            </View>
        )
    }
}