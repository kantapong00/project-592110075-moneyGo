import React, { Component } from 'react';
import { View, ImageBackground, TouchableOpacity, Image, Text, StyleSheet } from 'react-native';
import { Icon, InputItem, Button } from '@ant-design/react-native';
import loginBG from '../image/bg3.jpg'
import { WalletHeader, WalletBody, WalletBodyBox, MoneyBox } from '../style/BasicStyle';

export default class Wallet extends Component {
    render() {
        return (
            <ImageBackground source={loginBG} style={{ flex: 1 }}>
                <WalletHeader>
                    <TouchableOpacity>
                        <Icon name="arrow-left" size="lg" color="white" style={{ left: 10, top: 10 }} />
                    </TouchableOpacity>

                    <Image source={require('../image/piggy.gif')} style={{ width: 170, height: 170, left: 10, top: 10 }} />

                    <Text style={styles.money}>0.00</Text>

                    <MoneyBox style={{ top: -60 }}>
                        <Text style={{ color: 'white', fontSize: 20 }}>Wallet Transaction</Text>
                    </MoneyBox>

                </WalletHeader>
                
                <WalletBody>
                    <View style={styles.transactionBox}>

                    </View>
                    <Button style={styles.addIncome} activeStyle={{ backgroundColor: '#f16510' }}>Add Income</Button>
                    <Button style={styles.addExpenses} activeStyle={{ backgroundColor: 'black' }}>
                        <Text style={{ color: 'white' }}>Add Expenses</Text>
                    </Button>
                </WalletBody>
            </ImageBackground>
        )
    }
}

const styles = StyleSheet.create({
    transactionBox: {
        width: '90%',
        height: '70%',
        left: 18,
        top: 45,
        backgroundColor: 'rgba(255,255,255,0.8)'
    },
    addIncome: {
        width: 150,
        backgroundColor: '#f16510',
        borderColor: 'black',
        top: 60,
        left: 20
    },
    addExpenses: {
        width: 150,
        backgroundColor: 'black',
        top: 13,
        left: 180
    },
    money: {
        color: 'white',
        fontSize: 40,
        top: -70,
        left: 220
    }
})
