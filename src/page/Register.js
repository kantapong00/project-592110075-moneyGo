import React, { Component } from 'react';
import { View, Text, ImageBackground, Image, TouchableOpacity } from 'react-native';
import { style, Icon, Button, InputItem, StyleSheet } from '@ant-design/react-native'
import loginBG from '../image/bg3.jpg'
import { Container, InputBox, TextLogin, LoginButton } from '../style/BasicStyle'


export default class RegisterPage extends Component {

    success = () => {
        this.props.history.push('/')
    }
    cancel = () => {
        this.props.history.push('/')
    }

    render() {
        return (
            <ImageBackground source={loginBG} style={{ flex: 1 }}>
                <Container>

                    <Text style={{ color:'white',fontSize: 40, top: -80 }}>Sign Up</Text>

                    <View style={{ flexDirection: 'row', top: -50 }}>
                        <TextLogin style={{ marginLeft: 25 }}>E-mail :</TextLogin>
                        <InputBox>
                            <InputItem style={{ top: -2, fontSize: 15 }}/>
                        </InputBox>
                    </View>

                    <View style={{ flexDirection: 'row', top: -30 }}>
                        <TextLogin>Password :</TextLogin>
                        <InputBox>
                            <InputItem style={{ top: -2, fontSize: 15 }} />
                        </InputBox>
                    </View>

                    <View style={{ flexDirection: 'row', top: -10 }}>
                        <TextLogin>Firstname :</TextLogin>
                        <InputBox>
                            <InputItem style={{ top: -2, fontSize: 15 }} />
                        </InputBox>
                    </View>

                    <View style={{ flexDirection: 'row', top: 10 }}>
                        <TextLogin>Lastname :</TextLogin>
                        <InputBox>
                            <InputItem style={{ top: -2, fontSize: 15 }} />
                        </InputBox>
                    </View>

                    <Button style={{ top: 40, width: "90%", backgroundColor:'black', borderColor: 'rgba(0,0,0,0)' }} onPress={this.success}>
                        <Text style={{ color: 'orange' }}>Sign Up</Text>
                    </Button>
                    <Button style={{ top: 40, width: "90%", top: 50 }} onPress={this.cancel}>
                        <Text style={{ color: 'black' }}>Cancel</Text>
                    </Button>

                </Container>
            </ImageBackground>
        )
    }
}