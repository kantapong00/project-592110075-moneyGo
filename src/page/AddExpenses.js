import React, { Component } from 'react';
import { View, ImageBackground, TouchableOpacity, styles, Picker, Image, Text } from 'react-native';
import { Icon, InputItem } from '@ant-design/react-native';
import loginBG from '../image/addmoneyBG.jpg'
import { InputBox, Container, ButtonStyle, HeaderText, InputMoney, InputMemo } from '../style/BasicStyle';

export default class AddExpenses extends Component {
    state = {
        pickerValue: ''
    }

    gotoWallet = () => {
        return this.props.history.push('/Wallet')
    }
    back = () => {
        this.props.history.push('/Main')
    }
    clickMe = () => {
        var data = this.state.pickerValue
        if (data == "") {
            alert("Please Select a Option")
        } else {
            alert(data)
        }
    }

    render() {
        return (
            <ImageBackground source={loginBG} style={{ flex: 1 }}>
                <Container>
                    <TouchableOpacity style={{ flexDirection: 'row', top: -70, left: -110 }}>
                        <Image source={require('../image/back.png')} style={{ width: 50, height: 50 }} />
                        <Text style={{ color: 'black', top: 12, fontSize: 20 }}>Back</Text>
                    </TouchableOpacity>
                    <View>
                        <HeaderText style={{ bottom: 30, left: 65 }}>Add Expenses</HeaderText>
                    </View>
                    <View style={{ flexDirection: 'row', top: 10, left: -5 }}>
                        <HeaderText style={{ fontSize: 22, top: 10 }} >Money : </HeaderText>
                        <Image source={require('../image/coin.png')} style={{ width: 25, height: 30, top: 12, left: 20 }} />
                        <InputMoney style={{ left: -18 }}>
                            <InputItem
                                style={{ color: 'white' }}
                                type="number"
                                // value={this.state.money}
                                onChange={value => { this.setState({ money: value }) }}
                            />
                        </InputMoney>
                    </View>

                    <HeaderText style={{ top: -40, left: -110, fontSize: 22 }}>Type : </HeaderText>

                    <View style={{ borderWidth: 3, top: -80, borderRadius: 40, left: 30 }}>

                        <Picker
                            style={{ width: 212, color: 'white' }}
                            selectedValue={this.state.pickerValue}
                            onValueChange={(itemValue, itemIndex) => this.setState({ pickerValue: itemValue })}
                        >
                            <Picker.Item label="Select a option" value="" />
                            <Picker.Item label="Salary" value="Salary" />
                            <Picker.Item label="Gift" value="Gift" />
                        </Picker>
                        {/* <Button title="Click Me" onPress={this.clickMe} /> */}
                    </View>

                    <View style={{ flexDirection: 'row', top: -50 }}>
                        <HeaderText style={{ fontSize: 22, top: 10 }} >Note : </HeaderText>
                        <InputMemo>
                            <InputItem
                                style={{ color: 'white' }}
                                placeholder='Note Something'
                                // value={this.state.money}
                                onChange={value => { this.setState({ money: value }) }}
                            />
                        </InputMemo>
                    </View>

                    <TouchableOpacity style={{ flexDirection: 'row', top: 90, left: 100  }}>
                        <Text style={{ color: 'black', fontSize: 20 }}>Submit</Text>
                        <Image source={require('../image/plus.png')} style={{ width: 60, height: 60, top: -15 }} />
                    </TouchableOpacity>

                </Container>
            </ImageBackground>
        )
    }
}

