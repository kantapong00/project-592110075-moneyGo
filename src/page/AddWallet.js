import React, { Component } from 'react';
import { View, ImageBackground, TouchableOpacity, styles, Image } from 'react-native';
import { Icon, InputItem } from '@ant-design/react-native';
import loginBG from '../image/bg3.jpg'
import { InputBox, Container, ButtonStyle } from '../style/BasicStyle';


export default class AddWalletPage extends Component {

    state = {
        walletname: '',
        walletmoney: '0.00'
    }

    gotoWallet = () => {
        return this.props.history.push('/Wallet')
    }
    back = () => {
        this.props.history.push('/Main')
    }

    saveToMainPage = () => {
        let newId = 0
        if (this.props.wallets.length > 0)
            newId = parseInt(this.props.wallets[this.props.wallets.length - 1]._id, 10) + 1
        const wallet = {
            _id: newId.toString(),
            name: this.state.walletname === '' ? 'Wallet Name' : this.state.walletname,
            balance: this.state.walletmoney === '' ? 0.00 : parseFloat(this.state.walletmoney),
            transactions: []
        }
        this.props.addWallet(wallet)
        return this.props.history.push('/Wallet')
    }



    render() {
        return (
            <ImageBackground source={loginBG} style={{ flex: 1 }}>
                <Container>
                    <Image source={require('../image/wallet2.gif')} style={{ width: 120, height: 130, top: -60 }} />

                    <View style={{ flexDirection: 'row', top: -35 }}>
                        <Image source={require('../image/inputwallet.png')} style={{ width: 30, height: 30, top: 3 }} />
                        <InputBox>
                            <InputItem style={{ top: -2, fontSize: 15 }} placeholder="input your wallet name" />
                        </InputBox>
                    </View>

                    <View style={{ flexDirection: 'row' }}>

                        <TouchableOpacity onPress={this.saveToMainPage}>
                            <ButtonStyle style={{ marginRight: 40, backgroundColor: '#32CD32' }}>
                                <Icon name='check' size={20} color='white' />
                            </ButtonStyle>
                        </TouchableOpacity>

                        <TouchableOpacity onPress={this.back} >
                            <ButtonStyle style={{ backgroundColor: 'black' }}>
                                <Icon name='close' size={20} color='white' />
                            </ButtonStyle>
                        </TouchableOpacity>

                    </View>

                </Container>
            </ImageBackground>
        )
    }
}

