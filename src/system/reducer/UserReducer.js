export default (state = {}, action) => {
    switch (action.type) {
        case 'USER_LOGIN':
            console.log('login action:', action)
            return {
                email: action.user.email,
                password: action.user.password,
                firstName: action.user.firstName,
                lastName: action.user.lastName,
                token: action.user.token
            }
        case 'EDIT_USER':
            return {
                ...state,
                firstName: action.firstName,
                lastName: action.lastName,
                newPassword: action.newPassword
            }

        default:
            return state
    }
}
