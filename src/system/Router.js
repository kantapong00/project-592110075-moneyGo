import React, { Component } from 'react'
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import LoginPage from '../page/Login';
import RegisterPage from '../page/Register';
import MainPage from '../page/Main';
import AddWalletPage from '../page/AddWallet';
import ProfilePage from '../page/Profile';
import Wallet from '../page/Wallet';
import AddIncome from '../page/AddIncome';
import AddExpenses from '../page/AddExpenses';

class Router extends Component {
    render() {
        return (
            <NativeRouter>
                <Switch>
                    <Route exact path="/" component={LoginPage} />
                    <Route exact path="/Register" component={RegisterPage} />
                    <Route exact path="/Main" component={MainPage} />
                    <Route exact path="/Addwallet" component={AddWalletPage} />
                    <Route exact path="/Wallet" component={Wallet} />
                    <Route exact path="/AddIncome" component={AddIncome} />
                    <Route exact path="/AddExpenses" component={AddExpenses} />
                    <Route exact path="/Profile" component={ProfilePage} />
                    {/* <Route exact path="/Profile" component={ProfilePage} /> */}
                    <Redirect to="/" />
                </Switch>
            </NativeRouter>
        )
    }
}



export default Router
