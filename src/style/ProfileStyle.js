import styledCom from 'styled-components'
import { Button, InputItem, Icon } from '@ant-design/react-native'

export const IconCircle = styledCom.View`
    background-color: black;
    border-radius: 50;
    width: 50;
    height: 50;
    justify-content: center;
    align-items: center;
`