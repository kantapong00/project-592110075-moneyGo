import styledCom from 'styled-components'
// import { Dimensions } from 'react-native';
import { Button, InputItem, Icon } from '@ant-design/react-native'

export const Container = styledCom.View`
    flex : 1 ;
    align-items: center;
    justify-content: center;
`
export const InputBox = styledCom.View`
    margin-left: 10
    width: 65%;
    height: 90%;
    backgroundColor: rgba(255,255,255,0.5);
`
export const TextLogin = styledCom.Text`
    font-size: 15px;
    top: 7;
    color: white;
`
export const LoginButton = styledCom(Button)`
    width: 28%;
    borderColor: rgba(0,0,0,0);
`
export const MoneyBox = styledCom.View`
    backgroundColor: black;
    height: 20%;
    top: 50;
    align-items: center;
    justify-content: center;
`
export const MoneyText = styledCom.Text`
    font-size: 25px;
    color: orange;
`
export const Top = styledCom.View`
    align-items: center;
    justify-content: center;
`
export const Body = styledCom.View`
    flex: 3;
    justify-content: center;
    align-items: center;
`
export const ButtonStyle = styledCom.View`
    width: 50;
    height: 50; 
    border-radius: 80; 
    justify-content: center;
    align-items: center
`
export const HeaderText = styledCom.Text`
    font-size: 30px;
    color: white;
`
export const InputMoney = styledCom.View`
    paddingHorizontal: 25;
    fontSize: 25;
    width: 60%;
    height: 38%; 
    borderWidth: 3;
    borderColor: black;
    borderRadius: 40; 
    justifyContent: center;
    textAlign: center;
`
export const InputMemo = styledCom.View`
    paddingHorizontal: 2;
    fontSize: 25;
    width: 60%;
    height: 100%; 
    borderWidth: 3;
    borderColor: black;
    borderRadius: 40; 
`
export const WalletHeader = styledCom.View`
    flex: 1.5;
`
export const WalletBody = styledCom.View`
    flex: 3;
`
// export const BodyContainer = styledCom.View`
//     backgroundColor: #778899;
//     align-items: center;
//     justify-content: center;
//     flex : 1 ;
// `
// export const HeaderContainer = styledCom.View`
//     flex-direction: row;
//     align-items: center;
//     justify-content: center;
//     backgroundColor: #f2f2f2
//     height: 50
//     padding-top: 5;
//     padding-bottom: 5;
// `



