/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import LoginPage from './src/page/Login';
import RegisterPage from './src/page/Register';
import MainPage from './src/page/Main';
import Router from './src/system/Router';
import AddWalletPage from './src/page/AddWallet';
import ProfilePage from './src/page/Profile';
import AddIncome from './src/page/AddIncome';
import AddExpenses from './src/page/AddExpenses';
import Wallet from './src/page/Wallet';

AppRegistry.registerComponent(appName, () => Wallet);
